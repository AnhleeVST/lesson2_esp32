/* Blink Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "esp_system.h"
#include "string.h"
#include "input_iot.h"
#include "output_iot.h"

/* Can use project configuration menu (idf.py menuconfig) to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
#define BLINK_GPIO CONFIG_BLINK_GPIO
#define BTN_PIN    0
#define EX_UART_NUM UART_NUM_0
#define PATTERN_CHR_NUM    (3)         /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/

#define BUF_SIZE (1024)
#define RD_BUF_SIZE (BUF_SIZE)
static const char *TAG = "uart_events";
static QueueHandle_t uart0_queue;
uint8_t cmdStepOn = 0; 
uint8_t cmdStepOff = 0; 
uint8_t cntBTN = 0;
uint8_t LedState = 0; 
bool toggledst = false;  
void toggleLed(gpio_num_t gpio_num)
{ 
    if(toggledst) {
        gpio_set_level(gpio_num, 1);
        toggledst = false; 
    }
    else {
        gpio_set_level(gpio_num, 0);
        toggledst = true;
    }
}
static void uart_event_task(void *pvParameters)
{
    uart_event_t event;
    size_t buffered_size;
    uint8_t i; 
    uint8_t* dtmp = (uint8_t*) malloc(RD_BUF_SIZE);
    for(;;) {
        //Waiting for UART event.
        if(xQueueReceive(uart0_queue, (void * )&event, (portTickType)portMAX_DELAY)) {
            bzero(dtmp, RD_BUF_SIZE);
            // ESP_LOGI(TAG, "uart[%d] event:", EX_UART_NUM);
            switch(event.type) {
                //Event of UART receving data
                /*We'd better handler data event fast, there would be much more data events than
                other types of events. If we take too much time on data event, the queue might
                be full.*/
                case UART_DATA:
                    // ESP_LOGI(TAG, "[UART DATA]: %d", event.size);
                    uart_read_bytes(EX_UART_NUM, dtmp, event.size, portMAX_DELAY);
                    // ESP_LOGI(TAG, "[DATA EVT]:");
                    uart_write_bytes(EX_UART_NUM, (const char*) dtmp, event.size);
                    for(i =0; i<event.size;i++)
                    {
                         if(cmdStepOff == 2)
                        {
                            if(dtmp[i] == 0x46)
                            {
                                LedState = 0;
                                // ESP_LOGI(TAG, "STEP_OFF");
                                gpio_set_level(BLINK_GPIO, 0);
                            } 
                            cmdStepOff =0; 
                        }
                        if(cmdStepOff == 1)
                        {
                            if(dtmp[i] == 0x46) cmdStepOff = 2;
                            else cmdStepOff =0; 
                        }
                        if(cmdStepOn == 1)
                        {
                            if(dtmp[i] == 0x4e){
                                LedState = 1; 
                                // ESP_LOGI(TAG, "STEP_ON");
                                gpio_set_level(BLINK_GPIO, 1);
                            }
                            cmdStepOn = 0; 
                        }
                        if(dtmp[i] == 0x4f) {
                        cmdStepOn = 1; 
                        cmdStepOff = 1;
                        }
                        
                        //  ESP_LOGI(TAG, "[STEP OFF]: %d", cmdStepOff);
                        //  ESP_LOGI(TAG, "[STEP ON]: %d", cmdStepOn);
                    }
                    
                    break;
                //Event of HW FIFO overflow detected
                case UART_FIFO_OVF:
                    ESP_LOGI(TAG, "hw fifo overflow");
                    // If fifo overflow happened, you should consider adding flow control for your application.
                    // The ISR has already reset the rx FIFO,
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;
                //Event of UART ring buffer full
                case UART_BUFFER_FULL:
                    ESP_LOGI(TAG, "ring buffer full");
                    // If buffer full happened, you should consider encreasing your buffer size
                    // As an example, we directly flush the rx buffer here in order to read more data.
                    uart_flush_input(EX_UART_NUM);
                    xQueueReset(uart0_queue);
                    break;
                //Event of UART RX break detected
                case UART_BREAK:
                    ESP_LOGI(TAG, "uart rx break");
                    break;
                //Event of UART parity check error
                case UART_PARITY_ERR:
                    ESP_LOGI(TAG, "uart parity error");
                    break;
                //Event of UART frame error
                case UART_FRAME_ERR:
                    ESP_LOGI(TAG, "uart frame error");
                    break;
                //UART_PATTERN_DET
                case UART_PATTERN_DET:
                    uart_get_buffered_data_len(EX_UART_NUM, &buffered_size);
                    int pos = uart_pattern_pop_pos(EX_UART_NUM);
                    ESP_LOGI(TAG, "[UART PATTERN DETECTED] pos: %d, buffered size: %d", pos, buffered_size);
                    if (pos == -1) {
                        // There used to be a UART_PATTERN_DET event, but the pattern position queue is full so that it can not
                        // record the position. We should set a larger queue size.
                        // As an example, we directly flush the rx buffer here.
                        uart_flush_input(EX_UART_NUM);
                    } else {
                        uart_read_bytes(EX_UART_NUM, dtmp, pos, 100 / portTICK_PERIOD_MS);
                        uint8_t pat[PATTERN_CHR_NUM + 1];
                        memset(pat, 0, sizeof(pat));
                        uart_read_bytes(EX_UART_NUM, pat, PATTERN_CHR_NUM, 100 / portTICK_PERIOD_MS);
                        ESP_LOGI(TAG, "read data: %s", dtmp);
                        ESP_LOGI(TAG, "read pat : %s", pat);
                    }
                    break;
                //Others
                default:
                    ESP_LOGI(TAG, "uart event type: %d", event.type);
                    break;
            }
        }
    }
    free(dtmp);
    dtmp = NULL;
    vTaskDelete(NULL);
}

void btn_handler(void* arg)
{
  toggleLed(BLINK_GPIO);
}
void vTask1( void * pvParameters )
{

    for( ;; )

    {

        printf("Task 1\n");

        vTaskDelay(1000/portTICK_RATE_MS);

    }

}
void app_main(void)
{
    /* Configure the IOMUX register for pad BLINK_GPIO (some pads are
       muxed to GPIO on reset already, but some default to other
       functions and need to be switched to GPIO. Consult the
       Technical Reference for a list of pads and their default
       functions.)
    */
    output_io_create(BLINK_GPIO,0);
    output_io_set_level(BLINK_GPIO,0);
    // gpio_pad_select_gpio(BTN_PIN);
    // /* Set the GPIO as a push/pull output */
    // gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);
    // gpio_set_direction(BTN_PIN, GPIO_MODE_INPUT);
    // gpio_set_pull_mode(BTN_PIN,GPIO_PULLUP_ONLY);
    esp_log_level_set(TAG, ESP_LOG_INFO);

    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    //Install UART driver, and get the queue.
    uart_driver_install(EX_UART_NUM, BUF_SIZE * 2, BUF_SIZE * 2, 20, &uart0_queue, 0);
    uart_param_config(EX_UART_NUM, &uart_config);

    // //Set UART log level
    esp_log_level_set(TAG, ESP_LOG_INFO);
    //Set UART pins (using UART0 default pins ie no changes.)
    uart_set_pin(EX_UART_NUM, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);

    // //Set uart pattern detect function.
    uart_enable_pattern_det_baud_intr(EX_UART_NUM, '+', PATTERN_CHR_NUM, 9, 0, 0);
    //Reset the pattern queue length to record at most 20 pattern positions.
    uart_pattern_queue_reset(EX_UART_NUM, 20);
     xTaskCreate(uart_event_task, "uart_event_task", 2048, NULL, 12, NULL);
    input_io_create(GPIO_NUM_0,LO_TO_HI);
    input_set_callback(btn_handler);
    while(1) {
         xTaskCreate(

                    vTask1,       /* Function that implements the task. */

                    "vTask1",          /* Text name for the task. */

                    1024,      /* Stack size in words, not bytes. */

                    NULL,    /* Parameter passed into the task. */

                    4,/* Priority at which the task is created. */

                    NULL );      /* Used to pass out the created task's handle. */
    }
}
