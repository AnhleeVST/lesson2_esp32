#include <stdio.h>
#include <esp_log.h>
#include <driver/gpio.h>
#include "output_iot.h"

esp_err_t output_io_create(gpio_num_t gpio_num, output_level_t level)
{
 /* Configure power */
    gpio_config_t io_conf = {
        .mode = GPIO_MODE_OUTPUT,
    };
    uint64_t pin_mask = ((uint64_t)1 << gpio_num );
    io_conf.pin_bit_mask = pin_mask;
    /* Configure the GPIO */
    gpio_config(&io_conf);
    gpio_set_level(gpio_num, level);    
    return ESP_OK;
}

esp_err_t output_io_set_level(gpio_num_t gpio_num, output_level_t level)
{
    gpio_set_level(gpio_num, level);  
    return ESP_OK;  
}

void output_io_toggle(gpio_num_t gpio_num)
{
    uint8_t old_level = gpio_get_level(gpio_num);
     gpio_set_level(gpio_num, 1-old_level);
}