#ifndef OUTPUT_IO_H
#define OUTPUT_IO_H
#include "esp_err.h"
#include "hal/gpio_types.h"

typedef enum{
    OUTPUT_LEVEL_HIGH = 1,
    OUTPUT_LEVEL_LOW = 0
}   output_level_t;

esp_err_t output_io_create(gpio_num_t gpio_num, output_level_t level);
esp_err_t output_io_set_level(gpio_num_t gpio_num, output_level_t level);
void output_io_toggle(gpio_num_t gpio_num);
#endif
